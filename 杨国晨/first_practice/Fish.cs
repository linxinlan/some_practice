﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace first_practice
{
   public  class Fish:Animal
    {
        public override void behavior()
        {
            Console.WriteLine("鱼也会的行为");
            base.behavior();
            Console.WriteLine("鱼特有的行为吐泡泡");
        }
    }
}
