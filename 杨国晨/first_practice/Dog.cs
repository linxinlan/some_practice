﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace first_practice
{
   public  class Dog :Animal
    {
        public override void behavior()
        {
            Console.WriteLine("狗也会的行为");
            base.behavior();
            Console.WriteLine("狗特有的行为：咬人");
        }
       
    }
}
