﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animal
{
    public abstract class Animal
    {
        public string name { get; internal set; }
        public string color { get; internal set; }

        public virtual void Move()
        {
        Console.WriteLine("都具备的行为：移动");
        }
    }
}
