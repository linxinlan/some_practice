﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
   public class Car : Repair
    {
        public string Carname { get; set; }
        public string Carcolor { get; set; }
        public int Carwheels { get; set; }
         
        public  void Run()
        {
            if (Carwheels >= 4)
            {
                Console.WriteLine("可以跑");
            }
            else
            {
                Healcar();
            }
        }
    }
}
