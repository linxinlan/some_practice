﻿using System;

namespace Demo2
{
    class Program
    {
        static void Main(string[] args)
        {

            Dag dog = new Dag();
            dog.color();
            dog.name();
            dog.run();
            dog.ebite();

            Console.WriteLine("----------------------");

            Fish fish = new Fish();
            fish.bubbles();

        }
    }
}
