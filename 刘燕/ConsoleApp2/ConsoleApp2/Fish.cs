﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    public class Fish :Animal
    {
        public Fish()
        {
            Aname="鱼";
            Console.WriteLine($"请输入{Aname}的颜色:");
            this.Acolor=Console.ReadLine();
            this.Move();
        }
        public void bubbles()
        {
            Console.WriteLine($"{this.Acolor}色的{this.Aname}吐泡泡");
        }
    }
}
