﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp7
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("输入是什么动物：");

            string WhatAnimal = Console.ReadLine();

            if (WhatAnimal.Equals("狗"))
                {
                    Dog dog = new Dog();

                    Console.WriteLine("输入狗的名字：");
                    dog.Name = Console.ReadLine();

                    Console.WriteLine("输入狗的颜色：");
                    dog.Color = Console.ReadLine();

                    dog.Behavior();
            }
          

            if (WhatAnimal.Equals("猫"))
            {
                Fish flash = new Fish();

                Console.WriteLine("输入鱼的名字：");
                flash.Name = Console.ReadLine();

                Console.WriteLine("输入鱼的颜色：");
                flash.Color = Console.ReadLine();

                flash.Behavior();

            }
   
        }
    }
}
